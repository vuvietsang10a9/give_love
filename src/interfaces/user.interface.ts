export interface IUser {
  id: number;
  email: string;
  phone: string;
  username: string;
  fullName: string;
  dob?: string;
  address: string;
  role: string;
}
