export interface IExpense {
  description: string;
  money: number;
  dateTime: string;
  image: string;
  location: string;
  unit: string;
  giftname: string;
}
