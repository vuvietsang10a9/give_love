import { TextFieldProps } from "@atlaskit/textfield";
import { OptionType, SelectProps } from "@atlaskit/select";
import { RadioGroupProps } from "@atlaskit/radio/RadioGroup";
import { RangeProps } from "@atlaskit/range";
import { ToggleProps } from "@atlaskit/toggle/dist/types/types";
import { CheckboxProps } from "@atlaskit/checkbox";
import Textarea from "@atlaskit/textarea";
import {
  FieldProps,
  RangeFieldProps,
  CheckboxFieldProps,
  Field,
} from "@atlaskit/form";
import FormInput from "@components/FormInput";
import {
  DatePickerProps,
  DateTimePickerProps,
} from "@atlaskit/datetime-picker";
import {
  FieldErrors,
  FieldName,
  FieldPath,
  FieldValues,
  RegisterOptions,
} from "react-hook-form";

// const a: Adsd = { isRequired: true };
// export type AllAttrs = {
//   textfield: { inputProps: TextFieldProps; fieldProps: FieldType };
//   select: {
//     inputProps: SelectProps<any>;
//     fieldProps: FieldType;
//   };
//   radio: {
//     inputProps: RadioGroupProps;
//     fieldProps: FieldType;
//   };
//   range: {
//     inputProps: RangeProps;
//     fieldProps: Omit<RangeFieldProps, "children">;
//   };
//   toggle: {
//     inputProps: ToggleProps;
//     fieldProps: FieldType;
//   };
//   checkbox: {
//     inputProps: CheckboxProps;
//     fieldProps: Omit<CheckboxFieldProps, "children">;
//   };
//   date: {
//     inputProps: CheckboxProps;
//     fieldProps: Omit<CheckboxFieldProps, "children">;
//   };
// };
export type AllAttrs = {
  textfield: TextFieldProps;
  select: SelectProps<OptionType>;

  radio: RadioGroupProps;

  range: RangeProps;

  toggle: ToggleProps;

  checkbox: CheckboxProps;

  date: DatePickerProps;
  datetime: DateTimePickerProps;
  textarea: React.ComponentProps<typeof Textarea>;
  multiSelect: SelectProps<OptionType, true>;
};
type PropUpdate<T extends object> = {
  [K in keyof T]: { component: K; inputProps?: T[K] };
}[keyof T];

declare type FieldValuesFromFieldErrors<TFieldErrors> =
  TFieldErrors extends FieldErrors<infer TFieldValues> ? TFieldValues : never;

export type FormInputProps<
  TFieldValues extends FieldValues,
  TFieldName extends FieldPath<TFieldValues> = FieldPath<TFieldValues>
> = {
  // component: TComponent;
  className?: string;
  name: TFieldName;
  errorName?: FieldName<FieldValuesFromFieldErrors<FieldErrors<TFieldValues>>>;
  label: string;
  formProps?: RegisterOptions<TFieldValues, TFieldName>;
  helperText?: string;
  // inputProps?: PropUpdate<AllAttrs>;
} & PropUpdate<AllAttrs>;
