import { lazy } from "react";
import { RouteConfig } from "../../interfaces/routeConfig";
const Login = lazy(() => import("../pages/login"));
const Register = lazy(() => import("../pages/register"));
const Campaign = lazy(() => import("../pages/campain"));
const CampaignDetail = lazy(() => import("../pages/campaign-detail"));
const Expenses = lazy(() => import("../pages/expenses"));
const Account = lazy(() => import("../pages/account"));
const AccountDetail = lazy(() => import("../pages/account-detail"));
const AccountPersonalDetail = lazy(
  () => import("../pages/account-personal-detail")
);
const MemberInCampaignTable = lazy(() => import("../pages/memberInCampaign"));
const AddDonatorToCampaign = lazy(
  () => import("../pages/add-donator-toCampaign")
);
const DonatorInCampaignTable = lazy(() => import("../pages/donatorInCampaign"));

const routes: RouteConfig[] = [
  {
    path: "/",
    exact: true,
    component: Login,
  },
  {
    path: "/dashboard",
    exact: true,
    component: Campaign,
    isPrivate: true,
  },
  {
    path: "/campaign-details/:id",
    component: CampaignDetail,
    isPrivate: true,
  },
  {
    path: "/campaign-expense/:id",
    component: Expenses,
    isPrivate: true,
  },
  {
    path: "/account-register",
    exact: true,
    component: Register,
  },
  {
    path: "/account",
    exact: true,
    component: Account,
    isPrivate: true,
    accessRole: ["Admin"],
  },
  {
    path: "/account-details/:id",
    component: AccountDetail,
    isPrivate: true,
  },
  {
    path: "/account-personal-details/:id",
    component: AccountPersonalDetail,
    isPrivate: true,
  },
  {
    path: "/members-in-campaign/:id",
    component: MemberInCampaignTable,
    isPrivate: true,
  },
  {
    path: "/donator-in-campaign/:id",
    component: DonatorInCampaignTable,
    isPrivate: true,
  },
  {
    path: "/add-donator-toCampaign/:id",
    component: AddDonatorToCampaign,
    isPrivate: true,
  },
];
export default routes;
