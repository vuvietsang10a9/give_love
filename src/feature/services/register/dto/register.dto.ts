export interface RegisterDto {
  avatar: string;
  email: string;
  password: string;
  username: string;
  phone: string;
  roleName: string;
}
