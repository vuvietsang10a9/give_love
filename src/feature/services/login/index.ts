import { API } from "../../../config/axios";
import { LoginDto, LoginResponse } from "./dto/login.dto";

export const login = async (requestData: LoginDto) => {
  const {data} = await API.post<LoginResponse>("/auth/login", requestData);
  return data.data;
};
