import { PagiantionConstant } from "@defines/api";
import { API } from "../../../config/axios";
import { GetExpenseResponse } from "./dto/get-expense.dto";

interface Filter {
  pageSize?: number;
  pageNum?: number;
  startDate?: Date;
  endDate?: Date;
  name?: string;
  sort?: string;
}

export const getExpense = async (campaignId: string, filter?: Filter) => {
  const {
    pageNum = PagiantionConstant.defaultPageNum,
    pageSize = PagiantionConstant.defaultPageSize,
  } = filter || {};
  const { data } = await API.get<GetExpenseResponse>(`/expenses/${campaignId}`);
  return data.data;
};
