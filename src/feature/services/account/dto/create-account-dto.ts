import { ISuccessHttpResponse } from "src/interfaces/success_http_response.interface";
import { IUser } from "src/interfaces/user.interface";

export interface CreateAccountDto {
  username: string;
  password: string;
  email?: string;
  phone?: string;
  roleId: string;
}

export interface CreateAccountResponse extends ISuccessHttpResponse {
  data: IUser;
}
