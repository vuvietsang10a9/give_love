import { API } from "@config/axios";
import { PagiantionConstant } from "@defines/api";
import { ICampaign } from "src/interfaces/campaign.interface";
import {
  AddDonatorToCampaign,
  GetCampaignsByIdResponse,
  GetCampaignsResponse,
} from "./dto/get-campaign.dto";

interface Filter {
  name?: string;
  state?: string;
  pageNum: number;
  pageSize?: number;
  sort?: string;
}

export const getCampaigns = async (filter?: Filter) => {
  const {
    pageNum = PagiantionConstant.defaultPageNum,
    pageSize = PagiantionConstant.defaultPageSize,
  } = filter || {};
  const query = new URLSearchParams({
    pageNum: pageNum.toString(),
    pageSize: pageSize.toString(),
    name: "",
    sort: "id",
    search: `name:${filter?.name}`,
  });
  const { data } = await API.get<GetCampaignsResponse>(
    `/campaign?${query.toString()}&search=state:${filter?.state}`
  );
  return data.data;
};

export const getCampaignsForDonator = async (id: string) => {
  const { data } = await API.get<GetCampaignsByIdResponse>(
    `/campaign/donator/${id}`
  );
  return data;
};

export const getCampaign = async (id: string) => {
  const { data } = await API.get<GetCampaignsResponse>(
    `/campaign?pageNum=0&pageSize=10&sort=id&search=id:${id}`
  );

  return data.data;
};

export const addDonatorToCampaign = async (info: AddDonatorToCampaign) => {
  const { data } = await API.post<GetCampaignsResponse>(
    `/campaign/addDonator?donatorId=${info.donatorId}&campaignId=${info.campaignId}`
  );

  return data.data;
};
