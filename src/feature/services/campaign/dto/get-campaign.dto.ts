import { ICampaign } from "../../../../interfaces/campaign.interface";
import { ISuccessHttpResponse } from "../../../../interfaces/success_http_response.interface";
export interface GetCampaignsResponse extends ISuccessHttpResponse {
  data: { data: ICampaign[]; totalPage: number };
}

export interface GetCampaignsByIdResponse extends ISuccessHttpResponse {
  data: ICampaign[];
}

export interface AddDonatorToCampaign {
  donatorId: string;
  campaignId: string;
}
