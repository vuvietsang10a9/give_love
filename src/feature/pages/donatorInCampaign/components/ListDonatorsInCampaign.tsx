import Spiner from "@atlaskit/spinner";
import { useAppSelector } from "@components/hooks/reduxHook";
import React from "react";
import { shallowEqual } from "react-redux";
import { useParams } from "react-router";
import useDonatorInCampaign from "../hooks/useDonatorInCampaign";
import "../styles.css";
import MemberInCampaignTable from "./MemberInCampaignTable";

const ListDonatorsInCampaign = () => {
  const { id } = useParams<{ id: string }>();
  const { data, isLoading } = useDonatorInCampaign(id);
  const role = useAppSelector((state) => state.auth?.role, shallowEqual);

  // @ts-ignore
  if (!data) {
    return <Spiner />;
  }

  return (
    <>
      <MemberInCampaignTable data={data} isLoading={isLoading} role={role} />
    </>
  );
};

export default ListDonatorsInCampaign;
