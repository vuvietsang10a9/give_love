import Button from "@atlaskit/button";
import React, { useCallback, useState } from "react";
import { ModalTransition } from "@atlaskit/modal-dialog";
import Modal, {
  ModalBody,
  ModalHeader,
  ModalTitle,
} from "@atlaskit/modal-dialog";
import useAddDonator from "../../add-donator-toCampaign/hooks/useAddDonator";
import { useHistory, useParams } from "react-router";
const AddDonatorSection = () => {
  const [isOpen, setIsOpen] = useState(false);
  const history = useHistory();
  const openModal = useCallback(() => setIsOpen(true), []);
  const closeModal = useCallback(() => setIsOpen(false), []);
  const { id } = useParams<{ id: string }>();
  return (
    <div>
      <Button
        onClick={() => {
          history.goBack();
        }}
      >
        Trở về
      </Button>
      <Button
        appearance="primary"
        onClick={() => {
          history.replace(`/add-donator-toCampaign/${id}`);
        }}
      >
        {"Thêm mạnh thường quân"}
      </Button>
    </div>
  );
};

export default AddDonatorSection;
