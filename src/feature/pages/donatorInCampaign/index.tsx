import PageHeader from "@atlaskit/page-header";
import React, { useState } from "react";
import ListDonatorInCampaign from "./components/ListDonatorsInCampaign";
import Button from "@atlaskit/button";
import { useHistory } from "react-router-dom";
import AddDonatorSection from "./components/AddDonatorSection";

const Account = () => {
  const history = useHistory();
  return (
    <div>
      <PageHeader actions={<AddDonatorSection />}>
        Các mạnh thường quân đã đóng góp cho chiến dịch
      </PageHeader>

      <ListDonatorInCampaign />
    </div>
  );
};

export default Account;
