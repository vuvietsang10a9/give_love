import React from "react";
import Form, { ErrorMessage, FormFooter, FormHeader } from "@atlaskit/form";
import Button from "@atlaskit/button";
import { Controller, FormProvider, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { useHistory } from "react-router-dom";
import { RegisterDto } from "../../services/register/dto/register.dto";
import InputField from "../../../components/formInputs/InputField";
import PasswordField from "../../../components/formInputs/PasswordField";

const Register = () => {
  const history = useHistory();
  const schema = yup.object().shape({
    username: yup.string().required("Name is required"),

    password: yup.string().required("Password is required"),
    email: yup
      .string()
      .required("Email is required")
      .email("Email is wrong format"),
    phone: yup.string().required("Phone is required"),
  });

  const form = useForm<RegisterDto>({
    defaultValues: {
      avatar: "",
      email: "",
      password: "",
      username: "",
      phone: "",
    },
    resolver: yupResolver(schema),
  });
  const { handleSubmit, ...formFunc } = form;
  // const dispatch = useAppDispatch();
  return (
    <div className="flex items-center justify-center h-full min-h-screen py-2 mx-auto my-auto">
      <div className="w-full max-w-md p-5 rounded shadow-2xl">
        <Form
          onSubmit={(data) => {
            console.log("form data", data);
          }}
        >
          {({ formProps, dirty, submitting }) => (
            <form
              {...formProps}
              onSubmit={handleSubmit((data) => {
                // dispatch(login());
                history.push("/dashboard");
              })}
            >
              <div className="text-center">
                <FormHeader>
                  <div className="text-2xl font-medium text-gray-600">
                    Đăng kí tài khoản
                  </div>
                </FormHeader>
              </div>

              <FormProvider {...formFunc} handleSubmit={handleSubmit}>
                <InputField form={form} label="Username" name="username" />
                <PasswordField form={form} label="Password" name="password" />
                <InputField form={form} label="Email" name="email" />
                <InputField form={form} label="Phone" name="phone" />
              </FormProvider>

              <FormFooter>
                <Button
                  shouldFitContainer
                  type="submit"
                  appearance="primary"
                  isDisabled={submitting}
                >
                  Đăng ký
                </Button>
              </FormFooter>
            </form>
          )}
        </Form>
      </div>
    </div>
  );
};

export default Register;
