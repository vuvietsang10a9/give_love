import { useQuery } from "react-query";
import { getAccountByCampaignId } from "../../../services/account";

const useMemberInCampaign = (id: string) => {
  return useQuery([`userDetail${id}`, id], async () => {
    return getAccountByCampaignId(id);
  });
};

export default useMemberInCampaign;
