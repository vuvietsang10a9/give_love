import PageHeader from "@atlaskit/page-header";
import React, { useState } from "react";
import ListMemberInCampaign from "./components/ListMemberInCampaign";
import Button from "@atlaskit/button";
import { useHistory } from "react-router-dom";

const Account = () => {
  const history = useHistory();
  return (
    <div>
      <PageHeader
        actions={
          <Button
            onClick={() => {
              history.goBack();
            }}
          >
            Trở về
          </Button>
        }
      >
        Các thành viên tham gia chiến dịch
      </PageHeader>

      <ListMemberInCampaign />
    </div>
  );
};

export default Account;
