import { useAppSelector } from "@components/hooks/reduxHook";
import { useQuery } from "react-query";
import { shallowEqual } from "react-redux";
import { getAccounts } from "../../../services/account";

interface Filter {
  pageSize?: number;
  pageNum?: number;
  fullName?: string;
  role?: string;
  username?: string;
  phone?: string;
  email?: string;
  sort?: string;
}
const useAccounts = (filter?: Filter) => {
  return useQuery(["accounts", filter], async () => {
    return await getAccounts(filter);
  });
};

export default useAccounts;
