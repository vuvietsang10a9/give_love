import { useMutation, useQueryClient } from "react-query";
import { createAccount } from "../../../services/account";
import { CreateAccountDto } from "../../../services/account/dto/create-account-dto";

const useCreateAgency = () => {
  const queryClient = useQueryClient();
  return useMutation(
    async (data: CreateAccountDto) => {
      return await createAccount(data);
    },
    {
      onSettled: () => {
        queryClient.invalidateQueries("accounts");
      },
    }
  );
};

export default useCreateAgency;
