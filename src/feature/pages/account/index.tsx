import PageHeader from "@atlaskit/page-header";
import React, { useState } from "react";
import CreateAccountSection from "./components/CreateAccountSection";
import ListAccounts from "./components/ListAccounts";
import SearchSection from "./components/SearchSection";
const Account = () => {
  const [edit, setEdit] = useState(false);
  return (
    <div>
      {/* <PageHeader>
        <div className="flex justify-between items-end">
          <div>Danh sách tài khoản</div>
          <div>
            <SearchSection />
          </div>
        </div>
      </PageHeader>
      <div className="flex justify-end">
        <CreateAccountSection />
      </div>
      <ListAccounts /> */}

      <PageHeader actions={<CreateAccountSection />}>
        <div>
          <div>Danh sách tài khoản</div>
        </div>
      </PageHeader>
      <ListAccounts />
    </div>
  );
};

export default Account;
