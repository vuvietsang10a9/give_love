import React from "react";
import Button from "@atlaskit/button";
const Action = ({
  edit,
  setEdit,
}: {
  edit: boolean;
  setEdit: (edit: boolean) => void;
}) => {
  return edit ? null : (
    <Button appearance="primary" onClick={() => setEdit(true)}>
      Create account
    </Button>
  );
};

export default Action;
