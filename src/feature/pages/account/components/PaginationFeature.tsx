import React, { SyntheticEvent } from "react";
import Pagination from "@atlaskit/pagination";
import { UIAnalyticsEvent } from "@atlaskit/analytics-next";

interface SearchValue {
  fullName?: string;
  role?: string;
  username?: string;
  phone?: string;
  email?: string;
  value: string;
  pageNum: number;
}
const PaginationFeature = ({
  // page = 1,
  // setPage,
  search,
  setSearch,
  totalPage = 1,
}: {
  totalPage?: number;
  // page: number;
  // setPage: (data: number) => void;
  search: SearchValue;
  setSearch: React.Dispatch<(state: SearchValue) => SearchValue>;
}) => {
  const pageNumList = [];
  for (let index = 1; index <= totalPage; index++) {
    pageNumList.push(index);
  }

  return totalPage ? (
    <>
      <Pagination
        defaultSelectedIndex={1}
        pages={pageNumList}
        onChange={(
          event: SyntheticEvent<Element, Event>,
          page: number,
          analyticsEvent?: UIAnalyticsEvent | undefined
        ) => {
          setSearch(() => ({ ...search, pageNum: page - 1 }));
        }}
        selectedIndex={search.pageNum}
      />
    </>
  ) : null;
};

export default PaginationFeature;
