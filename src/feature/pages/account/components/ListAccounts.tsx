import Spiner from "@atlaskit/spinner";
import { nanoid } from "@reduxjs/toolkit";
import React, { useEffect, useState } from "react";
import useAccounts from "../hooks/useAccounts";
import "../styles.css";
import AccountTable from "./AccountTable";
import PaginationFeature from "./PaginationFeature";
import SearchSection from "./SearchSection";

interface SearchValue {
  fullName?: string;
  role?: string;
  username?: string;
  phone?: string;
  email?: string;
  value: string;
  pageNum: number;
}
const ListCampaign = () => {
  const [search, setSearch] = useState<SearchValue>({
    value: "",
    pageNum: 0,
    fullName: "",
    phone: "",
  });

  const { data, isLoading } = useAccounts(search);
  // @ts-ignore
  if (!data && !data?.data) {
    return <Spiner />;
  }

  const realData = [...data?.data].filter((account) => {
    if (account.role !== "Admin") return account;
  });

  return (
    <>
      <div>
        <SearchSection setSearch={setSearch} />
      </div>
      <AccountTable data={realData} isLoading={isLoading} />
      <div className="flex justify-center">
        <PaginationFeature
          // page={page}
          // setPage={setPage}
          search={search}
          setSearch={setSearch}
          totalPage={data?.totalPage}
        />
      </div>
    </>
  );
};

export default ListCampaign;
