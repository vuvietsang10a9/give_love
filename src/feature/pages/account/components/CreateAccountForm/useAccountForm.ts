import * as yup from "yup";
export interface User {
  username: "";
  password: "";
  email: "";
  phone: "";
  role: { label: string; value: string };
}
const useAccountForm = () => {
  const defaultValues: User = {
    username: "",
    password: "",
    email: "",
    phone: "",
    role: { label: "Thành viên", value: "3" },
  };
  const schema = yup.object().shape({
    username: yup
      .string()
      .required("Tên đăng nhập không được để trống")
      .matches(/^\S*$/, "Không thể chứa khoảng trống")
      .min(6, "Tên đăng nhập phải đủ 8 kí tự")
      .max(16, "Tên đăng nhập chỉ nhập được tối đa 16 kí tự"),
    email: yup
      .string()
      .required("Email không được để trống")
      .email("Không đúng định dạng email"),
    phone: yup
      .string()
      .required("Số điện thoại không được để trống")
      .matches(/^[0-9]+$/, "Không đúng định dạng")
      .min(10, "Số điện thoại phải đúng 10 chữ số")
      .max(10, "Số điện thoại phải đúng 10 chữ số"),
    password: yup
      .string()
      .required("Tên đăng nhập không được để trống")
      .matches(/^\S*$/, "Không thể chứa khoảng trống")
      .min(8, "Mật khẩu phải đủ 8 kí tự")
      .max(16, "Mật khẩu chỉ nhập được tối đa 16 kí tự"),
  });

  return { schema, defaultValues };
};

export default useAccountForm;
