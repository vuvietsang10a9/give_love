import Button from "@atlaskit/button";
import InputField from "@components/formInputs/InputField";
import { useAppDispatch } from "@components/hooks/reduxHook";
import React from "react";
import { FormProvider, useForm } from "react-hook-form";
import Form, { FormFooter } from "@atlaskit/form";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { SearchAccountDto } from "src/feature/services/account/dto/search-accounts-dto";
import { setValue } from "../../../slices/search";
import EditorSearchIcon from "@atlaskit/icon/glyph/editor/search";
import SearchIcon from "@atlaskit/icon/glyph/search";

interface SearchValue {
  fullName?: string;
  role?: string;
  username?: string;
  phone?: string;
  email?: string;
  value: string;
  pageNum: number;
}

interface myProps {
  setSearch: React.Dispatch<(state: SearchValue) => SearchValue>;
}

const SearchSection = ({ setSearch }: myProps) => {
  const form = useForm<SearchValue>({
    defaultValues: { value: "" },
  });

  const { handleSubmit, ...formFunc } = form;
  return (
    <>
      <Form
        onSubmit={(data) => {
          console.log("form data", data);
        }}
      >
        {({ formProps, dirty, submitting }) => (
          <form
            onSubmit={handleSubmit((searchValue: SearchValue) => {
              const submitData = {
                ...searchValue,
                email: searchValue.value,
                fullName: searchValue.value,
                phone: searchValue.value,
                role: searchValue.value,
                username: searchValue.value,
              };
              setSearch((state) => ({
                ...submitData,
                pageNum: 0,
              }));
              form.reset({
                value: submitData.phone,
              });
            })}
          >
            <div>
              <FormProvider {...formFunc} handleSubmit={handleSubmit}>
                <div className="grid grid-cols-12">
                  <div className="col-span-2">
                    <InputField
                      form={form}
                      label=""
                      name="value"
                      isCompact
                      placeholder="Nhập số điện thoại"
                    />
                  </div>
                  <div className="flex items-end">
                    <div className="col-span-3">
                      <Button
                        appearance="primary"
                        iconBefore={<SearchIcon label="search" />}
                        type="submit"
                      ></Button>
                    </div>
                  </div>
                </div>
              </FormProvider>
            </div>
            {/* <NetworkError error={error as any} /> */}
          </form>
        )}
      </Form>
    </>
  );
};

export default SearchSection;
