import Spiner from "@atlaskit/spinner";
import { useAppSelector } from "@components/hooks/reduxHook";
import React, { useEffect, useState } from "react";
import { shallowEqual } from "react-redux";
import { useParams } from "react-router";
import { IUser } from "src/interfaces/user.interface";
import useAllDonators from "../hooks/useAllDonator";
import useDonatorInCampaign from "../hooks/useDonatorInCampaign";
import "../styles.css";
import AllDonatorsTable from "./AllDonatorsTable";

const ListAllDonators = () => {
  const { id } = useParams<{ id: string }>();
  const { data, isLoading } = useAllDonators();
  const [realData, setRealData] = useState(data);

  const { data: donatorsInCampaign } = useDonatorInCampaign(id);
  let finalData: IUser[] | undefined = data;
  const role = useAppSelector((state) => state.auth?.role, shallowEqual);

  useEffect(() => {
    if (donatorsInCampaign) {
      finalData =
        data?.filter((x) => {
          let keep = true;
          for (
            let index = 0;
            index < donatorsInCampaign.length && keep;
            index++
          ) {
            if (x.id === donatorsInCampaign[index].id) {
              keep = false;
            }
          }
          if (keep) {
            return x;
          }
        }) || [];

      setRealData(finalData);
    }
  }, [donatorsInCampaign, isLoading]);

  // @ts-ignore
  if (!data) {
    return <Spiner />;
  }

  return (
    <>
      <AllDonatorsTable data={realData} role={role} isLoading={isLoading} />
    </>
  );
};

export default ListAllDonators;
