import { useMutation, useQueryClient } from "react-query";
import { AddDonatorToCampaign } from "src/feature/services/campaign/dto/get-campaign.dto";
import { addDonatorToCampaign } from "../../../services/campaign";

const useAddDonator = (campaignId: string) => {
  const queryClient = useQueryClient();
  return useMutation(
    async (data: AddDonatorToCampaign) => {
      return await addDonatorToCampaign(data);
    },
    {
      onSettled: () => {
        queryClient.invalidateQueries(`donatorInCampaign${campaignId}`);
      },
    }
  );
};

export default useAddDonator;
