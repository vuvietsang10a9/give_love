import { useAppSelector } from "@components/hooks/reduxHook";
import { useQuery } from "react-query";
import { shallowEqual } from "react-redux";
import { getAllDonators } from "../../../services/account";

interface Filter {
  pageSize?: number;
  pageNum?: number;
  fullName?: string;
  role?: string;
  username?: string;
  phone?: string;
  email?: string;
  sort?: string;
}
const useAllDonators = (filter?: Filter) => {
  return useQuery(["donators", filter], async () => {
    const data = await getAllDonators(filter);

    return data.data.filter((x) => {
      if (x.role === "Donator") return x;
    });
  });
};

export default useAllDonators;
