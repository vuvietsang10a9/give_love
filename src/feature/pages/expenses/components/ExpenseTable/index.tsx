import React, { useState } from "react";
import DynamicTable from "@atlaskit/dynamic-table";
import Spiner from "@atlaskit/spinner";
import DropdownMenu, {
  DropdownItemGroup,
  DropdownItem,
} from "@atlaskit/dropdown-menu";

import "../../styles.css";
import { RowType } from "@atlaskit/dynamic-table/dist/types/types";
import { useTranslation } from "react-i18next";
import { Link, useHistory } from "react-router-dom";
import MoreIcon from "@atlaskit/icon/glyph/more";
import { IExpense } from "src/interfaces/expense.interface";
import { createKey, numberWithCommas } from "../../../../../helpers/index";
import { formatDate } from "../../../../../helpers/index";
import TableEmptyView from "@components/TableEmptyView";

const ExpenseTable = ({
  data,
  isLoading,
}: {
  data?: IExpense[];
  isLoading: boolean;
}) => {
  const history = useHistory();
  if (!data) {
    return <Spiner />;
  }
  const [page, setPage] = useState(1);

  // const dataRes: ICampaign[] = !Array.isArray(data) ? data["parent"] : data;
  const rows = data?.map<RowType>((expense, index) => ({
    key: `row-${index}-${expense.image}`,
    className: "border-b border-gray-200",
    cells: [
      {
        key: createKey(expense.image),
        content: (
          <span className="flex items-center w-40">
            <div>
              <img
                className="image"
                src={expense.image}
                alt="Hình ảnh chi tiết"
              />
            </div>
          </span>
        ),
      },
      {
        content: (
          <div>
            <span>{numberWithCommas(expense.money)}</span>
          </div>
        ),
      },
      // {
      //   key: createKey(expense.description),
      //   content: (
      //     <div>
      //       <span>{expense.description + "cccccccccccccc"}</span>
      //     </div>
      //   ),
      // },
      {
        key: createKey(expense.giftname),
        content: (
          <div>
            <span>{expense.giftname}</span>
          </div>
        ),
      },
      {
        content: (
          <div>
            <span>{40}</span>
          </div>
        ),
      },
      {
        key: createKey(expense.unit),
        content: (
          <div>
            <span>{expense.unit}</span>
          </div>
        ),
      },

      {
        key: createKey(expense.location),
        content: (
          <div>
            <span>{expense.location}</span>
          </div>
        ),
      },

      {
        content: (
          <div>
            <span>{formatDate(expense.dateTime)}</span>
          </div>
        ),
      },
      //   {
      //     content: (
      //       <div className="flex justify-end pr-5">
      //         <DropdownMenu
      //           triggerButtonProps={{ iconBefore: <MoreIcon label="more" /> }}
      //           triggerType="button"
      //         >
      //           <DropdownItemGroup>
      //             <DropdownItem
      //               onClick={() => {
      //                 history.push(`/expense-details/${expense.id}`);
      //               }}
      //             >
      //               {"Chi tiết"}
      //             </DropdownItem>
      //           </DropdownItemGroup>
      //         </DropdownMenu>
      //       </div>
      //     ),
      //   },
    ],
  }));
  const createHead = (withWidth: boolean) => {
    return {
      cells: [
        {
          key: "expenseImage",
          content: "Hình ảnh",
          isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        {
          key: "money",
          content: "Số tiền chi (VND)",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        // {
        //   key: "description",
        //   content: "Mô tả",
        //   // isSortable: true,
        //   width: withWidth ? 50 : undefined,
        // },
        {
          key: "giftname",
          content: "Tên vật phẩm",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },

        {
          key: "quantity",
          content: "Số lượng",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        {
          key: "unit",
          content: "Đơn vị",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },

        {
          key: "location",
          content: "Địa điểm",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        {
          key: "dateTime",
          content: "Ngày phát sinh",
          isSortable: true,
          width: withWidth ? 35 : undefined,
        },
        // {
        //   key: "more",
        //   shouldTruncate: true,
        //   width: withWidth ? 35 : undefined,
        // },
      ],
    };
  };
  const head = createHead(true);
  return (
    <DynamicTable
      head={head}
      rows={rows}
      loadingSpinnerSize="large"
      rowsPerPage={10}
      isLoading={isLoading}
      isFixedSize
      defaultSortKey="money"
      defaultSortOrder="ASC"
      onSort={() => console.log("onSort")}
      onSetPage={() => console.log("onSort")}
      emptyView={<TableEmptyView text="Không có khoản chi nào" />}
    />
  );
};

export default ExpenseTable;
