import Button from "@atlaskit/button";
import * as FileSaver from "file-saver";
import React from "react";
import { useHistory, useParams } from "react-router";
import { ICampaign } from "src/interfaces/campaign.interface";
import * as XLSX from "xlsx";
import useCampaignDetail from "../../campaign-detail/hooks/useCampaignDetail";
import useExpenses from "../hooks/useExpenses";
import Spiner from "@atlaskit/spinner";

interface IXLSXExpense {
  "Tên vật phẩm": string;
  "Đơn vị": string;
  "Số lượng": string;
  "Số tiền chi": string;
  "Địa điểm": string;
  "Mô tả": string;

  "Ngày giờ thực hiện": string;
}

const ExportExcelSection = () => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const { data: campaignResponse, isLoading: isLoadingResponse } =
    useCampaignDetail(id);
  const { data: expenseList, isLoading } = useExpenses(id);

  const fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";

  const fileExtension = ".xlsx";

  const exportToCSV = () => {
    const realExpenseList = !expenseList
      ? [
          {
            "Mô tả": "",
            "Đơn vị": "",
            "Số lượng": "",
            "Số tiền chi": "",
            "Ngày giờ thực hiện": "",
            "Hình ảnh": "",
            "Tên vật phẩm": "",
            "Địa điểm": "",
          },
        ]
      : expenseList?.map((expense) => {
          return {
            "Tên vật phẩm": expense.giftname,
            "Đơn vị": expense.unit,
            "Số lượng": "40",
            "Số tiền chi": expense.money + "",
            "Địa điểm": expense.location,
            "Mô tả": expense.description,
            "Ngày giờ thực hiện": expense.dateTime,
          };
        });

    const header = Object.keys([
      "Tên vật phẩm",
      "Đơn vị",
      "Số lượng",
      "Số tiền chi",
      "Địa điểm",
      "Mô tả",
      "Ngày giờ thực hiện",
    ]);
    let wscols = [];
    for (var i = 0; i < header.length; i++) {
      // columns length added
      wscols.push({ wch: header[i].length + 20 });
    }

    const ws = XLSX.utils.json_to_sheet<IXLSXExpense>(realExpenseList);

    ws["!cols"] = wscols;

    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };

    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });

    const data = new Blob([excelBuffer], { type: fileType });

    FileSaver.saveAs(data, campaignResponse?.data[0].name + fileExtension);
  };
  return (
    <>
      <div>
        <div className="space-x-2">
          <Button
            onClick={() => {
              history.goBack();
            }}
          >
            Trở lại
          </Button>
          {isLoading || isLoadingResponse ? (
            <Spiner />
          ) : (
            <Button appearance="primary" onClick={() => exportToCSV()}>
              Xuất file excel
            </Button>
          )}
        </div>
      </div>
    </>
  );
};

export default ExportExcelSection;
