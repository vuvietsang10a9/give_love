import { useQuery } from "react-query";
import { getCampaign } from "../../../services/campaign";

const useCampaignDetail = (id: string) => {
  return useQuery(["userDetail", id], async () => {
    return getCampaign(id);
  });
};

export default useCampaignDetail;
