import { useMutation } from "react-query";
import { AxiosError } from "axios";
import { useHistory } from "react-router";
import { useAppDispatch } from "@components/hooks/reduxHook";
import { LoginDto } from "src/feature/services/login/dto/login.dto";
import { login as loginAction } from "../../../slices/auth";
import { ErrorHttpResponse } from "src/interfaces/error_http_response.interface";
import { login } from "../../../services/login";
const useLogin = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  return useMutation(
    async (data: LoginDto) => {
      return await login(data);
    },
    {
      onSuccess: (data) => {
        dispatch(loginAction(data));
        //because data:any
        history.push("/dashboard");
      },
      onError: (_: AxiosError<ErrorHttpResponse>) => {
        console.log(_, "errorrrrrrrrrrr");
      },
    }
  );
};

export default useLogin;
