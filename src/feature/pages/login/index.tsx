import React from "react";
import * as yup from "yup";
import LoadingButton from "@atlaskit/button/loading-button";

import Form, {
  ErrorMessage,
  FormFooter,
  FormHeader,
  FormSection,
} from "@atlaskit/form";
import { FormProvider, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from "react-router-dom";
import useLogin from "./hooks/useLogin";
import { useAppSelector } from "../../../components/hooks/reduxHook";
import { useHistory } from "react-router";
import InputField from "@components/formInputs/InputField";
import PasswordField from "@components/formInputs/PasswordField";
interface loginForm {
  username: string;
  password: string;
}

const Login = () => {
  const auth = useAppSelector((state) => state.auth.isAuth);
  if (auth) {
    const history = useHistory();
    history.replace("/dashboard");
  }
  const schema = yup.object().shape({
    username: yup.string().required("Vui Lòng Nhập tên tài khoản"),

    password: yup.string().required("Mật Khẩu Không Được Bỏ Trống"),
  });
  const form = useForm<loginForm>({
    defaultValues: {
      username: "Admin123",
      password: "123456789",
    },
    resolver: yupResolver(schema),
  });
  const { handleSubmit, ...formFunc } = form;

  const { mutate: login, isLoading, error } = useLogin();

  return (
    <div className="flex items-center justify-center flex-grow w-full h-full">
      <div className="w-full max-w-sm p-5 pb-4 m-auto space-y-8 bg-white rounded shadow-2xl ">
        <Form onSubmit={(data) => console.log("form data", data)}>
          {({ formProps, dirty, submitting }) => (
            <form
              {...formProps}
              onSubmit={handleSubmit((data) => {
                login(data);
              })}
            >
              <div className="text-center ">
                <FormHeader>
                  <div className="text-2xl font-medium text-gray-600">
                    Đăng nhập
                  </div>
                </FormHeader>
              </div>

              <FormSection>
                <FormProvider {...formFunc} handleSubmit={handleSubmit}>
                  <InputField form={form} label="Username" name="username" />
                  <PasswordField form={form} label="Password" name="password" />
                </FormProvider>
              </FormSection>

              <FormFooter>
                <LoadingButton
                  isLoading={isLoading}
                  type="submit"
                  shouldFitContainer
                  appearance="primary"
                  isDisabled={submitting}
                >
                  Đăng nhập
                </LoadingButton>
              </FormFooter>
              {!auth && error && (
                <ErrorMessage>Sai thông tin đăng nhập</ErrorMessage>
              )}
            </form>
          )}
        </Form>
        <div className="pb-4"></div>
        {/* <div className="flex justify-center">
          <Link to="/account-register" className="font-medium underline">
            Đăng kí tài khoản
          </Link>
        </div> */}
      </div>
    </div>
  );
};
export default Login;
