import Button from "@atlaskit/button";
import { useAppSelector } from "@components/hooks/reduxHook";
import React from "react";

import { useTranslation } from "react-i18next";
import { shallowEqual } from "react-redux";
import { useHistory } from "react-router";
const UpdateAccountSection = ({
  edit,
  setEdit,
}: {
  edit: boolean;
  setEdit: (edit: boolean) => void;
}) => {
  const history = useHistory();
  return (
    <>
      <div>
        <div className="space-x-2">
          <Button
            onClick={() => {
              history.goBack();
            }}
          >
            Trở lại
          </Button>
          {edit ? null : (
            <Button appearance="primary" onClick={() => setEdit(true)}>
              Cập nhật thông tin
            </Button>
          )}
        </div>
      </div>
    </>
  );
};

export default UpdateAccountSection;
