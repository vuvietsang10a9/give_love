import Breadcrumbs, { BreadcrumbsItem } from "@atlaskit/breadcrumbs";
import PageHeader from "@atlaskit/page-header";
import { useAppSelector } from "@components/hooks/reduxHook";
import React, { useState } from "react";
import { shallowEqual } from "react-redux";
import { Link, useLocation, useParams } from "react-router-dom";
import AccountInfoUpdate from "./components/AccountInfoUpdate";
import UpdateAccountSection from "./components/UpdateAccountSection";
const AccountPersonalDetail = () => {
  const [edit, setEdit] = useState(false);
  const role = useAppSelector((state) => state.auth?.role, shallowEqual);

  return (
    <div>
      <PageHeader
        actions={<UpdateAccountSection edit={edit} setEdit={setEdit} />}
      >
        Chi tiết tài khoản
      </PageHeader>
      <div className="space-y-3">
        <AccountInfoUpdate edit={edit} setEdit={setEdit} />
        {/* <ListDepartment /> */}
      </div>
    </div>
  );
};

export default AccountPersonalDetail;
