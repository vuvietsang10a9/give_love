import { useQuery } from "react-query";
import { getAccount } from "../../../services/account";

const useAccount = (id: string) => {
  return useQuery([`account${id}`], async () => {
    return await getAccount(id);
  });
};

export default useAccount;
