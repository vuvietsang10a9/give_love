import Breadcrumbs, { BreadcrumbsItem } from "@atlaskit/breadcrumbs";
import PageHeader from "@atlaskit/page-header";
import Spiner from "@atlaskit/spinner";
import { useAppSelector } from "@components/hooks/reduxHook";
import React, { useCallback, useState } from "react";
import { shallowEqual } from "react-redux";
import { Link, useParams } from "react-router-dom";
import AccountInfo from "./components/AccountInfo";
import UpdateAccountSection from "./components/UpdateAccountSection";
import UpdatePasswordSection from "./components/UpdatePasswordSection";
import useAccount from "./hooks/useAccount";
const AccountDetail = () => {
  const [edit, setEdit] = useState(false);
  const { id } = useParams<{ id: string }>();

  const { data, isLoading } = useAccount(id);

  return isLoading ? (
    <Spiner />
  ) : (
    <div>
      <PageHeader
        breadcrumbs={
          <Breadcrumbs>
            <BreadcrumbsItem
              href="/account"
              text="Danh sách thành viên"
              component={(props: any) => (
                <Link {...props} to="/account">
                  Danh sách thành viên
                </Link>
              )}
            />
            <BreadcrumbsItem text={data?.data[0].username || ""} />
          </Breadcrumbs>
        }
        actions={
          <UpdateAccountSection
            edit={edit}
            setEdit={setEdit}
            role={data?.data[0].role || ""}
          />
        }
      >
        Chi tiết tài khoản
      </PageHeader>

      <div className="space-y-3">
        <AccountInfo edit={edit} setEdit={setEdit} />
        {/* <ListDepartment /> */}
        {/* <UpdatePasswordSection /> */}
      </div>
    </div>
  );
};

export default AccountDetail;
