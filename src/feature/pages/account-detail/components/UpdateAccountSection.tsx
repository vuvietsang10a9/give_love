import Button from "@atlaskit/button";
import React from "react";
import Spiner from "@atlaskit/spinner";

import { useTranslation } from "react-i18next";
import { useHistory, useParams } from "react-router";
import useAccount from "../hooks/useAccount";
import useAccountById from "../hooks/useAccountById";
import { useAppSelector } from "@components/hooks/reduxHook";
import { shallowEqual } from "react-redux";
const UpdateAccountSection = ({
  role,
  edit,
  setEdit,
}: {
  role: string;
  edit: boolean;
  setEdit: (edit: boolean) => void;
}) => {
  const history = useHistory();
  return (
    <>
      <div>
        <div className="space-x-2">
          <Button
            onClick={() => {
              history.goBack();
            }}
          >
            Trở lại
          </Button>

          {role !== "Manager" && (
            <Button appearance="primary" onClick={() => setEdit(true)}>
              Cập nhật vai trò
            </Button>
          )}
        </div>
      </div>
    </>
  );
};

export default UpdateAccountSection;
