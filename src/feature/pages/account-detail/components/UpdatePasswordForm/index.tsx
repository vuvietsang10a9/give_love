import Button from "@atlaskit/button/loading-button";
import Form, { FormFooter } from "@atlaskit/form";
import PasswordField from "@components/formInputs/PasswordField";
import { yupResolver } from "@hookform/resolvers/yup";
import React, { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import useUpdatePassword from "../../hooks/useUpdatePassword";
import useAccountForm, { User } from "./useAccountForm";
import { useParams } from "react-router";

interface RegisForm {
  closeModal: () => void;
  openModalConfirm: () => void;
}

const UpdatePasswordForm: React.FC<RegisForm> = (props) => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const { defaultValues, schema } = useAccountForm();
  const { mutate: updatePassword, error } = useUpdatePassword();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const { closeModal, openModalConfirm } = props;
  const close = () => {
    closeModal();
  };

  const form = useForm<User>({
    defaultValues,
    resolver: yupResolver(schema),
  });

  const { handleSubmit, ...formFunc } = form;

  return (
    <Form
      onSubmit={(data) => {
        console.log("form data", data);
      }}
      isDisabled={isLoading}
    >
      {({ formProps, dirty, submitting }) => (
        <form
          {...formProps}
          onSubmit={handleSubmit((data: User) => {
            // history.push("/agency-details?edit=1");
            setIsLoading(true);
            updatePassword(
              { ...data, userId: id },
              {
                onSuccess: (data) => {
                  setTimeout(() => {
                    setIsLoading(false);
                    openModalConfirm();
                    closeModal();
                    history.push(`/account-details/${id}`);
                  }, 2000);
                },
              }
            );
          })}
        >
          <div className="">
            <FormProvider {...formFunc} handleSubmit={handleSubmit}>
              <div>
                <PasswordField
                  form={form}
                  label="Mật khẩu mới"
                  name="password"
                />
                <PasswordField
                  form={form}
                  label="Nhập lại mật khẩu"
                  name="retypedPassword"
                />
              </div>
            </FormProvider>
          </div>
          {/* <NetworkError error={error as any} /> */}
          <FormFooter>
            <div className="flex justify-center w-full pb-10">
              <div className="w-full mr-5">
                <Button
                  appearance="subtle"
                  shouldFitContainer
                  onClick={close}
                  isDisabled={isLoading}
                >
                  {"Hủy"}
                </Button>
              </div>
              <div className="w-full">
                <Button
                  type="submit"
                  shouldFitContainer
                  appearance="primary"
                  isLoading={isLoading}
                >
                  {"Xác nhận"}
                </Button>
              </div>
            </div>
          </FormFooter>
        </form>
      )}
    </Form>
  );
};

export default UpdatePasswordForm;
