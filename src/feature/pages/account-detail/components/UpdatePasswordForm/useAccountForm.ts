import * as yup from "yup";
export interface User {
  password: string;
  retypedPassword: string;
}
const useAccountForm = () => {
  const defaultValues: User = {
    password: "",
    retypedPassword: "",
  };
  const schema = yup.object().shape({
    password: yup
      .string()
      .required("Mật khẩu không được để trống")
      .matches(/^\S*$/, "Không thể chứa khoảng trống"),
    retypedPassword: yup
      .string()
      .required("Mật khẩu không được để trống")
      .oneOf([yup.ref("password")], "Mật khẩu nhập lại không đúng"),
  });

  return { schema, defaultValues };
};

export default useAccountForm;
