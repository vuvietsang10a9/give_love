import React from "react";
import DynamicTable from "@atlaskit/dynamic-table";
import Spiner from "@atlaskit/spinner";
import DropdownMenu, {
  DropdownItemGroup,
  DropdownItem,
} from "@atlaskit/dropdown-menu";

import "../../styles.css";
import { RowType } from "@atlaskit/dynamic-table/dist/types/types";
import { useTranslation } from "react-i18next";
import { Link, useHistory } from "react-router-dom";
import MoreIcon from "@atlaskit/icon/glyph/more";
import { ICampaign } from "src/interfaces/campaign.interface";
import { createKey, numberWithCommas } from "../../../../../helpers/index";
import { formatDate } from "../../../../../helpers/index";
import TableEmptyView from "@components/TableEmptyView";

const CampaignTable = ({
  data,
  isLoading,
  onDelete,
  autoPaging,
}: {
  data?: ICampaign[];
  isLoading: boolean;
  onDelete?: (id: String) => void;
  autoPaging: boolean;
}) => {
  const history = useHistory();
  if (!data) {
    return <Spiner />;
  }
  // const dataRes: ICampaign[] = !Array.isArray(data) ? data["parent"] : data;
  const onUpdate = () => {};
  const rows = data?.map<RowType>((campaign, index) => ({
    key: `row-${index}-${campaign.id}`,
    className: "border-b border-gray-200",
    cells: [
      {
        key: createKey(campaign.name),
        content: (
          <span className="flex items-center">
            <div>
              <Link to={`/campaign-details/${campaign.id}`} className="block">
                <p className="fullname">{campaign.name}</p>
              </Link>
            </div>
          </span>
        ),
      },
      {
        key: createKey(campaign.manager),
        content: (
          <div>
            <span>{campaign.manager}</span>
          </div>
        ),
      },
      {
        content: (
          <div>
            <span>{numberWithCommas(campaign.total)}</span>
          </div>
        ),
      },
      // {
      //   content: (
      //     <div>
      //       <span>{campaign.totalExpenses}</span>
      //     </div>
      //   ),
      // },
      // {
      //   content: (
      //     <div>
      //       <span>{campaign.remainMoney}</span>
      //     </div>
      //   ),
      // },
      {
        key: createKey(campaign.location),
        content: (
          <div>
            <span>{campaign.location}</span>
          </div>
        ),
      },
      // {
      //   key: createKey(campaign.description),
      //   content: (
      //     <div>
      //       <span>{campaign.description}</span>
      //     </div>
      //   ),
      // },

      {
        content: (
          <div>
            <span>{formatDate(campaign.startDate)}</span>
          </div>
        ),
      },
      {
        content: (
          <div>
            <span>{formatDate(campaign.endDate)}</span>
          </div>
        ),
      },
      // {
      //   content: (
      //     <div>
      //       <span>{campaign.state ? "Đang diễn ra" : "Đã hoàn thành"}</span>
      //     </div>
      //   ),
      // },
      {
        content: (
          <div className="flex justify-end pr-5">
            <DropdownMenu
              triggerButtonProps={{ iconBefore: <MoreIcon label="more" /> }}
              triggerType="button"
            >
              <DropdownItemGroup>
                {onDelete && (
                  <DropdownItem onClick={() => onDelete(campaign.id + "")}>
                    {"Xóa khỏi danh sách"}
                  </DropdownItem>
                )}
                <DropdownItem
                  onClick={() => {
                    history.push(`/campaign-details/${campaign.id}`);
                  }}
                >
                  {"Chi tiết"}
                </DropdownItem>
              </DropdownItemGroup>
            </DropdownMenu>
          </div>
        ),
      },
    ],
  }));
  const createHead = (withWidth: boolean) => {
    return {
      cells: [
        {
          key: "campaignName",
          content: "Tên chiến dịch",
          isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        {
          key: "manager",
          content: "Trưởng chiến dịch",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        {
          key: "total",
          content: "Tiền quỹ ban đầu (VND)",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        // {
        //   key: "expense",
        //   content: "Chi phí",
        //   // isSortable: true,
        //   width: withWidth ? 50 : undefined,
        // },
        // {
        //   key: "remain",
        //   content: "Số dư",
        //   // isSortable: true,
        //   width: withWidth ? 50 : undefined,
        // },
        {
          key: "location",
          content: "Nơi diễn ra",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        // {
        //   key: "description",
        //   content: "Mô tả",
        //   // isSortable: true,
        //   width: withWidth ? 50 : undefined,
        // },

        {
          key: "createDate",
          content: "Ngày bắt đầu",
          isSortable: true,
          width: withWidth ? 35 : undefined,
        },
        {
          key: "endDate",
          content: "Ngày kết thúc",
          isSortable: true,
          width: withWidth ? 35 : undefined,
        },
        // {
        //   key: "state",
        //   content: "Tình trạng",
        //   isSortable: true,
        //   width: withWidth ? 35 : undefined,
        // },
        {
          key: "more",
          shouldTruncate: true,
          width: withWidth ? 35 : undefined,
        },
      ],
    };
  };
  const head = createHead(true);
  return autoPaging ? (
    <DynamicTable
      head={head}
      rows={rows}
      loadingSpinnerSize="large"
      isLoading={isLoading}
      isFixedSize
      defaultSortKey="campaignName"
      defaultSortOrder="ASC"
      rowsPerPage={10}
      onSort={() => console.log("onSort")}
      onSetPage={() => console.log("onSetPage")}
      defaultPage={1}
      emptyView={
        <TableEmptyView text="Không có chiến dịch nào trong danh sách" />
      }
    />
  ) : (
    <DynamicTable
      head={head}
      rows={rows}
      loadingSpinnerSize="large"
      isLoading={isLoading}
      isFixedSize
      defaultSortKey="campaignName"
      defaultSortOrder="ASC"
      onSort={() => console.log("onSort")}
      onSetPage={() => console.log("onSetPage")}
      emptyView={
        <TableEmptyView text="Không có chiến dịch nào trong danh sách" />
      }
    />
  );
};

export default CampaignTable;
