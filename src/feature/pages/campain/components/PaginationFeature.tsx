import React, { SyntheticEvent } from "react";
import Pagination from "@atlaskit/pagination";
import { UIAnalyticsEvent } from "@atlaskit/analytics-next";
import { nanoid } from "@reduxjs/toolkit";
interface SearchValue {
  name?: string;
  state?: string;
  pageNum: number;
  pageSize?: number;
  sort?: string;
}

const PaginationFeature = ({
  search,
  setSearch,
  totalPage = 1,
}: {
  totalPage?: number;
  search: SearchValue;
  setSearch: React.Dispatch<(state: SearchValue) => SearchValue>;
}) => {
  const pageNumList = [];
  for (let index = 1; index <= totalPage; index++) {
    pageNumList.push(index);
  }

  return totalPage ? (
    <>
      <Pagination
        key={nanoid()}
        defaultSelectedIndex={0}
        pages={pageNumList}
        onChange={(
          event: SyntheticEvent<Element, Event>,
          page: number,
          analyticsEvent?: UIAnalyticsEvent | undefined
        ) => {
          setSearch(() => ({ ...search, pageNum: page - 1 }));
        }}
        selectedIndex={search.pageNum}
      />
    </>
  ) : null;
};

export default PaginationFeature;
