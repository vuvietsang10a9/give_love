import Spiner from "@atlaskit/spinner";
import React, { useEffect, useState } from "react";
import useCampaigns from "../hooks/useCampaigns";
import "../styles.css";
import CampaignTable from "./CampaignTable";
import PaginationFeature from "./PaginationFeature";
import SearchSection from "./SearchSection";
interface SearchValue {
  name?: string;
  state?: string;
  pageNum: number;
  pageSize?: number;
  sort?: string;
}

const ListCampaign = () => {
  const [search, setSearch] = useState<SearchValue>({
    name: "",
    state: "true",
    pageNum: 0,
  });
  const { data, isLoading } = useCampaigns(search);

  // @ts-ignore
  if (!data && !data?.data) {
    return <Spiner />;
  }

  return (
    <>
      <div>
        <SearchSection setSearch={setSearch} />
      </div>
      <CampaignTable
        autoPaging={false}
        data={data?.data}
        isLoading={isLoading}
      />
      <div className="flex justify-center">
        <PaginationFeature
          search={search}
          setSearch={setSearch}
          totalPage={data?.totalPage}
        />
      </div>
    </>
  );
};

export default ListCampaign;
