import Button from "@atlaskit/button";
import InputField from "@components/formInputs/InputField";
import { useAppDispatch } from "@components/hooks/reduxHook";
import React from "react";
import { FormProvider, useForm } from "react-hook-form";
import Form, { FormFooter } from "@atlaskit/form";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { SearchAccountDto } from "src/feature/services/account/dto/search-accounts-dto";
import { setValue } from "../../../slices/search";
import EditorSearchIcon from "@atlaskit/icon/glyph/editor/search";
import SearchIcon from "@atlaskit/icon/glyph/search";
import SelectField from "@components/formInputs/SelectField";

interface SearchValue {
  name?: string;
  state?: string;
  selectState?: { label: string; value: string };

  pageNum: number;
  pageSize?: number;
  sort?: string;
}

interface myProps {
  setSearch: React.Dispatch<(state: SearchValue) => SearchValue>;
}

const SearchSection = ({ setSearch }: myProps) => {
  const form = useForm<SearchValue>({
    defaultValues: {
      name: "",
      state: "",
      selectState: { label: "Trạng thái - Đang diễn ra", value: "true" },
    },
  });
  const selectionData = [
    { key: "Trạng thái - Đang diễn ra", value: "true" },
    { key: "Trạng thái - Đã hoàn thành", value: "false" },
  ];

  const { handleSubmit, ...formFunc } = form;

  return (
    <>
      <Form
        onSubmit={(data) => {
          console.log("form data", data);
        }}
      >
        {({ formProps, dirty, submitting }) => (
          <form
            onSubmit={handleSubmit((searchValue: SearchValue) => {
              const submitData = {
                ...searchValue,
                name: searchValue.name,
                state: searchValue.selectState?.value,
                pageNum: 0,
                //pageNum set to 0 when search
              };

              setSearch((state) => ({
                ...submitData,
              }));
              form.reset({
                name: submitData.name,
                selectState: submitData.selectState,
              });
            })}
          >
            <div>
              <FormProvider {...formFunc} handleSubmit={handleSubmit}>
                <div className="grid grid-cols-12">
                  <div className=" col-span-2">
                    <InputField
                      form={form}
                      label=""
                      name="name"
                      isCompact
                      placeholder="Nhập tên chiến dịch"
                    />
                  </div>
                  <div className="flex items-end">
                    <div className="">
                      <Button
                        appearance="primary"
                        iconBefore={<SearchIcon label="search" />}
                        type="submit"
                      ></Button>
                    </div>
                  </div>
                  <div className="col-end-13 col-span-2">
                    <SelectField
                      form={form}
                      label=""
                      data={selectionData}
                      name="selectState"
                      isCompact={true}
                    />
                  </div>
                </div>
              </FormProvider>
            </div>
            {/* <NetworkError error={error as any} /> */}
          </form>
        )}
      </Form>
    </>
  );
};

export default SearchSection;
