import PageHeader from "@atlaskit/page-header";
import React from "react";
import ListCampaign from "./components/ListCampaign";
import { useTranslation } from "react-i18next";
import { useAppSelector } from "@components/hooks/reduxHook";
import { shallowEqual } from "react-redux";
import ListCampaignForDonator from "./components/ListCampaignForDonator";

const Campaign = () => {
  const role = useAppSelector((state) => state.auth?.role, shallowEqual);
  return (
    <div>
      <PageHeader>
        Danh sách chiến dịch{role !== "Admin" && " đã quyên góp"}
      </PageHeader>
      {/* actions={<CreateDepartmentSection />} */}
      {role === "Admin" ? <ListCampaign /> : <ListCampaignForDonator />}
    </div>
  );
};

export default Campaign;
