import { useQuery } from "react-query";
import { getCampaign } from "src/feature/services/campaign";

const useCampaign = (agencyId: string) => {
  return useQuery(["campaign", agencyId], async () => {
    return await getCampaign(agencyId);
  });
};

export default useCampaign;
