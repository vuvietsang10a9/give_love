const aliases = (prefix = `./src`) => ({
  "@pages": `${prefix}/pages`,
  "@hooks": `${prefix}/hooks`,
  "@components": `${prefix}/components`,
  "@config": `${prefix}/config`,
  "@services": `${prefix}/services`,
  "@models": `${prefix}/modles`,
  "@helper": `${prefix}/helper`,
  "@assets": `${prefix}/assets`,
  "@routes": `${prefix}/routes`,
  "@modules": `${prefix}/modules`,
  "@app": `${prefix}/app`,
  "@defines": `${prefix}/defines`,
});
module.exports = aliases;
