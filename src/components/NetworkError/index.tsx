import { ErrorMessage } from "@atlaskit/form";
import { AxiosError } from "axios";
import React from "react";
import { ErrorHttpResponse } from "src/interfaces/error_http_response.interface";

const NetworkError = ({
  error,
}: {
  error?: AxiosError<ErrorHttpResponse> | null;
}) => {
  return error ? <ErrorMessage>Đã có lỗi xảy ra</ErrorMessage> : null;
};

export default NetworkError;
