import { ErrorMessage, Field } from "@atlaskit/form";
import Textfield from "@atlaskit/textfield";
import React, { FC, Fragment } from "react";
import { Controller, UseFormReturn } from "react-hook-form";
import { css } from "@emotion/core";

interface Props {
  name: string;
  label: string;
  disabled?: boolean;
  value: string;
}

const bigFontStyles = css({
  // container style
  padding: 10,
  "& > [data-ds--text-field--container]": {
    // input style
    fontSize: 30,
  },
});
const TextView: FC<Props> = ({ name, label, value, disabled }: Props) => {
  return (
    <Field label={label} name={name}>
      {() => (
        <Textfield
          label={name}
          disabled={disabled}
          isReadOnly={true}
          value={value}
          css="padding:100px"
        />
      )}
    </Field>
  );
};
export default TextView;
