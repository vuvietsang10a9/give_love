import { ErrorMessage, Field } from "@atlaskit/form";
import Textfield from "@atlaskit/textfield";
import { max } from "moment";
import React, { FC, Fragment } from "react";
import { Controller, UseFormReturn } from "react-hook-form";

interface Props {
  form: UseFormReturn<any>;
  name: string;
  label: string;
  disabled?: boolean;
  placeholder?: string;
  isReadOnly?: boolean;
  isCompact?: boolean;
}
const InputField: FC<Props> = ({
  form,
  name,
  label,
  disabled,
  placeholder,
  isReadOnly,
  isCompact,
}: Props) => {
  const { formState } = form;
  const hasError = formState.errors[name] && formState.touchedFields[name];
  return (
    <Field label={label} name={name}>
      {() => (
        <Controller
          name={name}
          control={form.control}
          render={({ field }) => (
            <Fragment>
              <Textfield
                label={name}
                {...field}
                isInvalid={hasError}
                isReadOnly={isReadOnly}
                isDisabled={disabled}
                isCompact={isCompact}
                placeholder={placeholder}
              />
              {hasError && (
                <ErrorMessage>{formState.errors[name]?.message}</ErrorMessage>
              )}
            </Fragment>
          )}
        />
      )}
    </Field>
  );
};
export default InputField;
