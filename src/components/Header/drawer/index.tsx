import React from "react";
import Drawer from "@atlaskit/drawer";
import SidebarOverrideComponent from "./SidebarOverrideComponent";
import SidebarContent from "./SidebarContent";
import { AppSwitcher as AppSwitcherDefault } from "@atlaskit/atlassian-navigation";
export default function AppSwitcher() {
  const [isOpen, setOpen] = React.useState(false);
  return (
    <div css={{ padding: "2rem" }}>
      <Drawer
        onClose={() => {
          setOpen(false);
        }}
        onCloseComplete={() => {
          setOpen(false);
        }}
        isOpen={isOpen}
        width="narrow"
        overrides={{
          Sidebar: {
            component: SidebarOverrideComponent,
          },
        }}
      >
        <SidebarContent />
      </Drawer>

      <AppSwitcherDefault
        tooltip="Đổi sang"
        onClick={() => {
          setOpen(true);
        }}
      />
    </div>
  );
}
