import moment from "moment";

export function createKey(input: string) {
  return input ? input.replace(/^(the|a|an)/, "").replace(/\s/g, "") : input;
}
export const formatDate = (date: Date | string) => {
  return moment(date).format("HH:mm | DD-MM-YYYY");
};

export const numberWithCommas = (x: number) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
